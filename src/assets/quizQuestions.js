let quizQuestions = [
    {
        "question": "What is Tolkiens full name?",
        "choices": [
            "Jason Regenald Ronald Tolkien",
            "John Ronald Reuel Tolkien",
            "Jacob Robert Russel Tolkien",
            "Jackson Reese Renat Tolkien"
        ],
        "answer": "John Ronald Reuel Tolkien"
    },
    {
        "question": "How was the universe created in the Silmarillion?",
        "choices": [
            "Dance",
            "Prayer",
            "Music",
            "Thought"
        ],
        "answer": "Music"
    },
    {
        "question": "What is the name of the world?",
        "choices": [
            "ëa",
            "Arda",
            "Middle Earth",
            "The Grey Havens"
        ],
        "answer": "Arda"
    },
    {
        "question": "What is the gift given to the elves by Eru?",
        "choices": [
            "Immortality",
            "'Rebirth' in Valinor after death",
            "Beauty and grace",
            "Knowledge"
        ],
        "answer": "'Rebirth' in Valinor after death"
    },
    {
        "question": "What is the 'proper' name for the elves?",
        "choices": [
            "Quendi",
            "Eldar",
            "Sindar",
            "Avari"
        ],
        "answer": "Quendi"
    },
    {
        "question": "What is another name for Valinor?",
        "choices": [
            "Arnor",
            "Heaven",
            "Harad",
            "The Undying Lands"
        ],
        "answer": "The Undying Lands"
    },
    {
        "question": "What is the gift given to men by Eru?",
        "choices": [
            "Mortality",
            "Fearlessness and an undying thirst for adventure",
            "The unknown after death",
            "Bravery and Courage"
        ],
        "answer": "The unknown after death"
    },
    {
        "question": "Where did the Hobbits come from?",
        "choices": [
            "They where born from Arda",
            "No one knows, the Hobbits forgot",
            "They evolved from men who just wanted to be left alone",
            "They evolved from elves who just wanted to be left alone"
        ],
        "answer": "No one knows, the Hobbits forgot"
    },
    {
        "question": "Who was the oldest living elf?",
        "choices": [
            "Tom Bombadil",
            "Círdan the Shipwright",
            "Elrond",
            "Galadriel"
        ],
        "answer": "Círdan the Shipwright"
    },
    {
        "question": "Who was Melkors apprentice?",
        "choices": [
            "Ulmo",
            "Mandos",
            "Morgoth",
            "Sauron"
        ],
        "answer": "Sauron"
    }
]

export default quizQuestions;
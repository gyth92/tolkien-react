import React, { Component } from 'react';
import './App.css';
import WelcomeComponent from "./components/WelcomePage/WelcomePage";
import QuizComponent from "./components/Quiz/Quiz";
import WelcomeBackground from "./assets/tolkien_trivia_intro.gif";
// import QuizBackground from "./assets/aTURT51.jpg";
import quizQuestions from "./assets/quizQuestions";
import Quiz from './components/Quiz/Quiz';

class App extends Component {
  constructor(props) {
    super();
    this.state = {
      currentComponent: 0,
      currentQuestion: 0

    }
    this.nextComponent = this.nextComponent.bind(this);
  }
  render() {

    if(this.state.currentComponent === 0) {
        return (
          <WelcomeComponent backgroundImage={WelcomeBackground} nextComponent={this.nextComponent}/>
        );
    } else if (this.state.currentComponent === 1) {
      return (
        <QuizComponent questions={quizQuestions}/>
      )
    }
  }

  nextComponent(event) {
    this.setState((prevState) => ({
      currentComponent: prevState.currentComponent + 1
  }));
    console.log('>>', this.state.currentComponent);
  };

}

export default App;

import React from "react";
import "./Welcome.css";

class WelcomePage extends React.Component{
    constructor(props) {
        super();
        this.state = {
            showQuiz: false
        };
    }

    render() {
        return (
            <div className="welcome__container">
                <img className="welcome__background--gif" src={this.props.backgroundImage}/>
                <div className="welcome__background--quiz">
                    <button className="btn-hover" onClick={this.props.nextComponent} >
                        Start the Quiz
                    </button>
                </div>
            </div>
        )
    }
}

export default WelcomePage;